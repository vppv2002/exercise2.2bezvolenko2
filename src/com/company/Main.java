package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String Privat = "ПриватБанк";
        String Oshad = "ОщадБанк";
        String PUMB = "ПУМБ";
        String USD = "USD";
        String EUR = "EUR";
        String RUB = "RUB";

        float coursePrivatBankUSD = 28.5f;
        float coursePrivatBankEUR = 31.55f;
        float coursePrivatBankRUB = 0.365f;
        float courseOshadBankUSD = 28.65f;
        float courseOshadBankEUR = 31.2f;
        float courseOshadBankRUB = 0.38f;
        float coursePUMBUSD = 28.5f;
        float coursePUMBEUR = 31.5f;
        float coursePUMBRUB = 0.355f;
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите количесвто денег, которое хотите обменять: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Введите банк через который хотите совершить обмен (ПриватБанк, ОщадБанк или ПУМБ): ");
        String bank = scan.nextLine();

        System.out.println("Введите валюту (USD, EUR или RUB): ");
        String currency = scan.nextLine();

        if (Privat.equalsIgnoreCase(bank)) {
            if (USD.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / coursePrivatBankUSD));
            } else if (EUR.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / coursePrivatBankEUR));
            } else if (RUB.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / coursePrivatBankRUB));
            } else {
                System.err.println("Невозможно сделать обмен " + currency);
            }
        } else if (Oshad.equalsIgnoreCase(bank)) {
            if (USD.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / courseOshadBankUSD));
            } else if (EUR.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / courseOshadBankEUR));
            } else if (RUB.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / courseOshadBankRUB));
            } else {
                System.err.println("Невозможно сделать обмен " + currency);
            }
        } else if (PUMB.equalsIgnoreCase(bank)) {
            if (USD.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / coursePUMBUSD));
            } else if (EUR.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / coursePUMBEUR));
            } else if (RUB.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / coursePUMBRUB));
            } else {
                System.err.println("Невозможно сделать обмен " + currency);
            }
        } else {
            System.err.println("Невозможно сделать обмен " + bank);
        }
    }
}